#!/usr/bin/env bash

## configuration de wiki.org 
# configuration du serveur dns

himage dwikiorg mkdir -p /etc/named
hcp wiki.org/named.conf dwikiorg:/etc/.
hcp wiki.org/* dwikiorg:/etc/named/.
himage dwikiorg rm /etc/named/named.conf 


## configuration de iut.re 
# configuration du serveur dns

himage diutre mkdir -p /etc/named
hcp iut.re/named.conf diutre:/etc/.
hcp iut.re/* diutre:/etc/named/.
himage diutre rm /etc/named/named.conf 


## configuration de pc1
# resolv.conf

hcp pc1/resolv.conf pc1:/etc/.

## configuration de rt.iut.re
## configuration du serveur dns

himage drtiutre mkdir -p /etc/named
hcp rt.iut.re/named.conf drtiutre:/etc/.
hcp rt.iut.re/* drtiutre:/etc/named/.
himage drtiutre rm /etc/named/named.conf

## configuration de pc2
# resolv.conf
hcp pc2/resolv.conf pc2:/etc/.
